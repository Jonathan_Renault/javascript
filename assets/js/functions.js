function energieCinetique(masse, vitesse){
    return 0.5 * masse * Math.pow(Math.abs(vitesse),2);
}

function calculFactorielle(x){
    if(x < 0) {
        alert('un nombre positif, michel');
        return false
    }

    else {
        if (x == 0)
            return 1 ;
         else
            return x * calculFactorielle(x-1);
    }
}

function tabtolist(tab){

    let result = "";

    if(Array.isArray(tab)){
        result = "<ul>";

        for( let i = 0; i < tab.length; i++){
            result += "<li>";
            result += tab[i];
            result += "</li>";
        }
        result+= "</ul>";
    }
    else {
        result = "<h2>Marche Pas</h2>"
    } return result;
}